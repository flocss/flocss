<?php $id="price"; ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/layout/header.php'); ?>
			<div class="l-breadcrumb">
				<div class="l-container">
					<ul>
						<li><a href="/index.php">ホーム</a></li>
						<li>価格表</li>
					</ul>
				</div>
			</div>
			<div class="l-container">
				<!-- code this here -->
				<!--////////////////-->
				<div class="l-main">
					<div class="l-conts">
						<!-- code this here -->
						<div class="p-price-list">
							<div class="font">
								<div class="c-title">格安お得なトラック積み詰め放題 </div>
							</div>
							<div class="c-box">
								<div class="c-box-title">押し入れ〜1K程度の小さなお部屋の片付けにおすすめ</div>
								<div class="c-box-content">
									<div class="c-box-left">
										<div class="c-box-font">
											荷台容積<br/>約<span>2.5</span>平米  
										</div>
										<img src="/images/index/car1.png">
										<img src="/images/price_list/price_01.png">
									</div>
									<div class="c-box-right">
										<span>軽（平）トラック1台積み詰め放題</span>
										<p>￥19,000-</p>
										<div class="c-box-none">押し入れ〜1K程度の小さなお部屋を片付けるのに、<br/>おすすめのパックです。</div>
										<div class="c-box-list">1Fの作業員1名（助手1名5,000円〜）</div>
										<div class="c-box-list">2F〜最大25,000円</div>
										<div class="c-box-red">※家電・鋳物のみなら￥<span>16,000-</span></div>
										※不用品回収作業60分程度を基本料金の最低目安にしております。
									</div>
									<div class="c-box-buttom"><p>回収一例</p>シングルベッド（脚付）/カラーボックス（3段）/センターテーブル/物干し竿 /石油ファンヒーター /衣装ケース(3段)/掃除機/コンポ/スーツケース/段ボール箱×1/電子レンジ/電気ポット/ギター/2ドア冷蔵庫/全自動洗濯機/薄型テレビ</div>
								</div>
							</div>
							<div class="c-box">
								<div class="c-box-title">1K〜1DK程度のお部屋の片付けにおすすめ</div>
								<div class="c-box-content">
									<div class="c-box-left">
										<div class="c-box-font">
											荷台容積<br/>約<span>3.5</span>平米  
										</div>
										<img src="/images/index/car2.png">
										<img src="/images/price_list/price_02.png">
									</div>
									<div class="c-box-right">
										<span>1トン平トラック1台積み詰め放題</span>
										<p>￥29,000-</p>
										<div class="c-box-none">1K～１DK程度のお部屋を片付けるのに、おすすめのパック<br/>です。</div>
										<div class="c-box-list">1Fの作業員1名（助手1名5,000円〜）</div>
										<div class="c-box-list">2F〜最大35,000円</div>
										※不用品回収作業60分程度を基本料金の最低目安にしております。
									</div>
									<div class="c-box-buttom"><p>回収一例</p>シングルベッド（脚付）/カラーボックス（3段）/テレビボード/オーブンレンジ/電気ポット/掃除機/木製デスク/キャビネット/整理箪笥/スーツケース/2ドア冷蔵庫/全自動洗濯機/薄型テレビ/エアコン/ゴミ袋(45L)×2/その他…</div>
								</div>
							</div>
							<div class="c-box">
								<div class="c-box-title">1DK〜2DK程度のお部屋の片付けにおすすめ</div>
								<div class="c-box-content">
									<div class="c-box-left">
										<div class="c-box-font">
											荷台容積<br/>約<span>5.0</span>平米  
										</div>
										<img src="/images/index/car3.png">
										<img src="/images/price_list/price_03.png">
									</div>
									<div class="c-box-right">
										<span>2トン平トラック1台積み詰め放題</span>
										<p>￥40,000-</p>
										<div class="c-box-none">1DK～2DK程度のお部屋を片付けるのに、おすすめのパックです。</div>
										<div class="c-box-list">1Fの作業員1名（助手1名5,000円〜）</div>
										<div class="c-box-list">2F〜最大50,000円</div>
										※不用品回収作業90分程度を基本料金の最低目安にしております。
									</div>
									<div class="c-box-buttom"><p>回収一例</p>シングルベッド（脚付）/扇風機 /カラーボックス（3段）/スーツケース /テレビボード/スノーボード/物干し竿/整理箪笥/石油ファンヒーター/木製デスク/掃除機/キャビネット/コンポ/布団（1組）/デスクトップパソコン/ダンボール箱×2/インクジェットプリンター/ごみ袋（45ℓ）×2/オーブンレンジ/2ドア冷蔵庫(109ℓ)/全自動洗濯機(4.2kg)/薄型テレビ/エアコン/その他…</div>
								</div>
							</div>
							<div class="c-box">
								<div class="c-box-title">2DK〜2LDK程度のお部屋の片付けにおすすめ</div>
								<div class="c-box-content">
									<div class="c-box-left">
										<div class="c-box-font">
											荷台容積<br/>約<span>10</span>平米  
										</div>
										<img src="/images/index/car4.png">
										<img src="/images/price_list/price_04.png">
									</div>
									<div class="c-box-right">
										<span>2トン箱トラック1台積み詰め放題</span>
										<p>￥80,000-</p>
										<div class="c-box-none">2DK～2LDK程度のお部屋を片付けるのにおすすめのパックです。冷蔵庫、洗濯機、テレビ、エアコン、パソコンも一緒に回収できます。<br/>おすすめのパックです。</div>
										<div class="c-box-list">1Fの作業員2名（助手1名5,000円〜）</div>
										<div class="c-box-list">2F〜最大95,000円</div>
										※不用品回収作業120分程度を基本料金の最低目安にしております。
									</div>
									<div class="c-box-buttom"><p>回収一例</p>ダブルベッド/DVDプレーヤー/カラーボックス（3段）/コンポ/サイドボード/エアコン/物干し竿/扇風機/石油ファンヒーター/空気清浄機/掃除機/食器棚 /シーリングライト/二人掛けソファー/デスクトップパソコン/センターテーブル/インクジェットプリンター/布団（1組）/オーブンレンジ/ダンボール箱×4/電気ポット/ごみ袋（45ℓ）×3/ガステーブル（2口）/6ドア冷蔵庫(450ℓ)/全自動洗濯機(7.0kg)/薄型テレビ/その他…</div>
								</div>
							</div>
							<div class="c-box">
								<div class="c-box-title">押し入れ〜1K程度の小さなお部屋の片付けにおすすめ</div>
								<div class="c-box-content">
									<div class="c-box-left">
										<div class="c-box-font">
											荷台容積<br/>約<span>2.5</span>平米  
										</div>
										<img src="/images/index/car1.png">
										<img src="/images/price_list/price_05.png">
									</div>
									<div class="c-box-right">
										<span>軽トラ1台 鋳物・家電のみなら</span>
										<p>￥16,000-</p>
										<span>軽トラ1台 半分の不用品回収</span>
										<p>￥19,000-</p>
										<div class="c-box-none">そんなに多くの容量はいらない‥という方向けに、お得なハーフプランもございます。2トントラックでもお安くできますので、お気軽にご相談下さい</div>
										
										※軽トラ1台半分の不用品回収9,000円の料金は東京・千葉・埼玉の一部地域限定のプランとなっております。
									</div>
								</div>
							</div>
							<div class="c-content">
								<div class="c-title__title">ご利用時の注意</div>
								<div class="content">
									<p>申込み時に搬出経路、階数、品物をお聞きしますが、状況によりオプション料金が掛かる事が有ります。</p>
									<p>危険物などは事前にお客様で分別していただく必要が有ります（ライター、マッチ、スプレー缶など）</p>
									<p>危険物、医療廃棄物、生ゴミ、動物の死骸、血液などが付着したものはお引き受けできません。</p>
									<p>金庫やピアノなどの特殊重量物、建築系廃棄物、消火器、可燃物、液体、食品、土砂石などは定額パックの対象外
									(別途料金)となります。</p>
									<p>回収の都合上、記載のトラックより大型の車両での回収になる場合が有ります。</p>
									<p>テレビ、冷蔵庫、ソファー、ソファーベッドは各１台までの回収が基本パックとなっております。２台目以降の回収は別途料金が発生する場合がございますので、予めご相談ください。</p>
								</div>
							</div>
							<div class="font">
								<div class="c-title">料金の計算方法 </div>
								<span>リサイクルマスター英雄は明朗会計です！<br/>基本料金2000円に下記個別回収料金を足してください。<br/>個別回収料金が8000円を越す場合は基本料金は無料とさせて頂きます！<br/>多数の回収品が有る場合はトラック積み放題プランの方がお得な場合がありますのでご比較ください。</span>
							</div>
							<div class="c-content">
								<div class="c-title__title">ご利用時の注意</div>
								<div class="content">
									<img src="/images/price_list/price_06.png">
								</div>
							</div>
						</div>
						<!-- code end here -->

							
						<!--////////////////-->
					</div>
					<?php include($_SERVER['DOCUMENT_ROOT'] . '/layout/sibar.php'); ?>
				</div>
				<?php include($_SERVER['DOCUMENT_ROOT'] . '/layout/footer.php'); ?>
			</div>

