<?php $id="flow"; ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/layout/header.php'); ?>
			<div class="l-breadcrumb">
				<div class="l-container">
					<ul>
						<li><a href="/index.php">ホーム</a></li>
						<li>買取・回収の流れ</li>
					</ul>
				</div>
			</div>
			<div class="l-container">
				<!-- code this here -->
				<!--////////////////-->
				<div class="l-main">
					<div class="l-conts">
						<!-- code this here -->
						<div class="p-flow">
							<div class="font">
								<div class="c-title">買取・回収の流れ</div>
								<p class="c-content">
									<span>無料お見積り</span><img src="	/images/flow/company_2.png">
									<span>買取・回収<br/>のご契約 </span><img src="/images/flow/company_2.png">
									<span>回収作業</span>
								</span>
							</div>
							<div class="c-box">
								<div class="c-box-left">
									<p>無料お見積り</p>
									<span>まずはお見積りをさせていただきます。相談・お見積りは無料！即日・夜間でも対応が可能です！</span>
									<div class="c-box-left__div">
										<div class="c-box-left__title">お気軽にお問い合わせ下さい</div>
										<div class="c-box-left__content">
											<img src="/images/flow/company_4.png">
											<span>0120-485-888</span> 携帯・PHSからもOK!<br/>お電話がつながりにくい場合  070-2177-7772 
											<p>24時間対応</p>
										</div>
									</div>
								</div>
								<div class="c-box-right">
									<img src="/images/flow/company_3.png">
								</div>
							</div>
							<div class="c-box">
								<div class="c-box-img"><img src="/images/flow/company_5.png"></div>
								<div class="c-box-left">
									<p>買取・回収のご契約</p>
									<span>ご契約後、御希望の回収日時にお伺い致します。 </span>
								</div>
								<div class="c-box-right">
									<img src="/images/flow/company_6.png">
								</div>
							</div>
							<div class="c-box">
								<div class="c-box-img"><img src="/images/flow/company_5.png"></div>
								<div class="c-box-left">
									<p>回収作業</p>
									<span>回収作業終了後、ご契約いただいた金額でご精算させていただきます。 </span>
								</div>
								<div class="c-box-right">
									<img src="/images/flow/company_7.png">
								</div>
							</div>
						</div>
						<!-- code end here -->

							<div class="font">
								<div class="c-title">初めての方へ</div>
								<span>業務用厨房機器・店舗用品・オフィス機器・オフィス用品・PC・ゲーム・家電家具等。リサイクルマスター英雄では法人のお客様から個人のお客様まで幅広く様々な品を承ります。回収品目に関しましてはお気軽にご相談下さい。 </span>
							</div>
							<div class="c-content">
								<div class="c-title__title">回収品目例</div>
								<div class="content">
									<ul>
										<li>洗浄機</li>
										<li>作業台</li>
										<li>業務用冷蔵庫</li>
										<li>シンク</li>
										<li>ガスレンジ</li>
										<li>薬味入れ</li>
										<li>寸銅鍋</li>
										<li>製氷機</li>

										<li>フライヤー</li>
										<li>電子レンジ</li>
										<li>ミキサー</li>
										<li>スライサー</li>
										<li>ショーケース</li>
										<li>コールドテーブル</li>
										<li>小物</li>
										<li>蔵庫</li>

										<li>自動販売機</li>
										<li>券売機 </li>
										<li>ショーケース</li>
										<li>ラック</li>
										<li>レジ</li>
										<li>食器</li>
										<li>OA機器</li>
										<li>パソコン</li>

										<li>机（デスク）</li>
										<li>椅子（チェア）</li>
										<li>カウンター</li>
										<li>ロッカー</li>
										<li>ビジネスフォン</li>
										<li>テーブル</li>
										<li>テレビ</li>
										<li>掃除機</li>

										<li>ソファ</li>
										<li>棚</li>
										<li>タンス</li>
										<li>生活用品</li>
										<li>その他不用品など</li>
									</ul>
									<div class="c-img">
										<img src="/images/index/c_img_1.png">
										<img src="/images/index/c_img_2.png">
										<img src="/images/index/c_img_3.png">
										<img src="/images/index/c_img_4.png">
										<img src="/images/index/c_img_6.png">
										<img src="/images/index/c_img_7.png">
									</div>
									<div class="z">他にもいろいろお取り扱いしております！</div>
								</div>
							</div>
							<div class="c-title">対応地域一覧</div>
							<div class="map_footer"><img src="/images/index_map.png"></div>
							<div class="c-c red">
								<span class="title_red title_t">東京都</span>
								<p>23区・中央区・港区・新宿区・文京区・台東区・千代田区・墨田区・江東区・品川区・目黒区・大田区・世田谷区・渋谷区・中野区・杉並区・豊島区・北区・荒川区・板橋区・練馬区・足立区・葛飾区・江戸川区・八王子市・田無市・立川市・武蔵野市・三鷹市・府中市・昭島市・調布市・町田市・小金井市・小平市・日野市・東村山市・国分寺市・国立市・西東京市・福生市・狛江市・東大和市・清瀬市・東久留米市・武蔵村山市・多摩市・稲城市・あきる野市・羽村市・青梅市 その他・・・ </p>
							</div>
							<div class="c-c yellow">
								<span class="title_yellow title_t">東京都</span>
								<p>23区・中央区・港区・新宿区・文京区・台東区・千代田区・墨田区・江東区・品川区・目黒区・大田区・世田谷区・渋谷区・中野区・杉並区・豊島区・北区・荒川区・板橋区・練馬区・足立区・葛飾区・江戸川区・八王子市・田無市・立川市・武蔵野市・三鷹市・府中市・昭島市・調布市・町田市・小金井市・小平市・日野市・東村山市・国分寺市・国立市・西東京市・福生市・狛江市・東大和市・清瀬市・東久留米市・武蔵村山市・多摩市・稲城市・あきる野市・羽村市・青梅市 その他・・・ </p>
							</div>
							<div class="c-c xam">
								<span class="title_xam title_t">東京都</span>
								<p>23区・中央区・港区・新宿区・文京区・台東区・千代田区・墨田区・江東区・品川区・目黒区・大田区・世田谷区・渋谷区・中野区・杉並区・豊島区・北区・荒川区・板橋区・練馬区・足立区・葛飾区・江戸川区・八王子市・田無市・立川市・武蔵野市・三鷹市・府中市・昭島市・調布市・町田市・小金井市・小平市・日野市・東村山市・国分寺市・国立市・西東京市・福生市・狛江市・東大和市・清瀬市・東久留米市・武蔵村山市・多摩市・稲城市・あきる野市・羽村市・青梅市 その他・・・ </p>
							</div>
							<p class="r">その他神奈川県・茨城県の一部地域も対応しておりますので、ご相談ください。 </p>
						<!--////////////////-->
					</div>
					<?php include($_SERVER['DOCUMENT_ROOT'] . '/layout/sibar.php'); ?>
				</div>
				<?php include($_SERVER['DOCUMENT_ROOT'] . '/layout/footer.php'); ?>
			</div>

