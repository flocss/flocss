<?php $id="about"; ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/layout/header.php'); ?>
			<div class="l-breadcrumb">
				<div class="l-container">
					<ul>
						<li><a href="/index.php">ホーム</a></li>
						<li>会社概要</li>
					</ul>
				</div>
			</div>
			<div class="l-container">
				<!-- code this here -->
				<!--////////////////-->
				<div class="l-main">
					<div class="l-conts">
						<!-- code this here -->
						<div class="p-contact">
							<div class="font">
								<div class="c-title">初めての方へ</div>
							</div>
							<table class="c-table">
								<tr>
									<td class="c-table-box">会社名</td>
									<td class="c-table-box-1">リサイクルマスター英雄</td>
								</tr>
								<tr>
									<td class="c-table-box">代表者</td>
									<td class="c-table-box-1">斉藤　誠人</td>
								</tr>
								<tr>
									<td class="c-table-box">古物商許可証</td>
									<td class="c-table-box-1">第985号</td>
								</tr>
								<tr>
									<td class="c-table-box">事務所所在地</td>
									<td class="c-table-box-1">京都板橋区小豆沢2-21-2<br/>千葉県市川市日の出22-1<br/>千葉県松戸市和名ヶ谷1333<br/>茨城県下妻市五箇530-8</td>
								</tr>
								<tr>
									<td class="c-table-box">電話番号(フリーダイヤル)</td>
									<td class="c-table-box-1">0120-485-888</td>
								</tr>
								<tr>
									<td class="c-table-box">携帯電話</td>
									<td class="c-table-box-1">070-2177-7772</td>
								</tr>
							</table>
						</div>
						<!-- code end here -->
							<div class="font">
								<div class="c-title">対応地域一覧</div>
							</div>
							<div class="map_footer"><img src="/images/index_map.png"></div>
							<div class="c-c red">
								<span class="title_red title_t">東京都</span>
								<p>23区・中央区・港区・新宿区・文京区・台東区・千代田区・墨田区・江東区・品川区・目黒区・大田区・世田谷区・渋谷区・中野区・杉並区・豊島区・北区・荒川区・板橋区・練馬区・足立区・葛飾区・江戸川区・八王子市・田無市・立川市・武蔵野市・三鷹市・府中市・昭島市・調布市・町田市・小金井市・小平市・日野市・東村山市・国分寺市・国立市・西東京市・福生市・狛江市・東大和市・清瀬市・東久留米市・武蔵村山市・多摩市・稲城市・あきる野市・羽村市・青梅市 その他・・・ </p>
							</div>
							<div class="c-c yellow">
								<span class="title_yellow title_t">東京都</span>
								<p>23区・中央区・港区・新宿区・文京区・台東区・千代田区・墨田区・江東区・品川区・目黒区・大田区・世田谷区・渋谷区・中野区・杉並区・豊島区・北区・荒川区・板橋区・練馬区・足立区・葛飾区・江戸川区・八王子市・田無市・立川市・武蔵野市・三鷹市・府中市・昭島市・調布市・町田市・小金井市・小平市・日野市・東村山市・国分寺市・国立市・西東京市・福生市・狛江市・東大和市・清瀬市・東久留米市・武蔵村山市・多摩市・稲城市・あきる野市・羽村市・青梅市 その他・・・ </p>
							</div>
							<div class="c-c xam">
								<span class="title_xam title_t">東京都</span>
								<p>23区・中央区・港区・新宿区・文京区・台東区・千代田区・墨田区・江東区・品川区・目黒区・大田区・世田谷区・渋谷区・中野区・杉並区・豊島区・北区・荒川区・板橋区・練馬区・足立区・葛飾区・江戸川区・八王子市・田無市・立川市・武蔵野市・三鷹市・府中市・昭島市・調布市・町田市・小金井市・小平市・日野市・東村山市・国分寺市・国立市・西東京市・福生市・狛江市・東大和市・清瀬市・東久留米市・武蔵村山市・多摩市・稲城市・あきる野市・羽村市・青梅市 その他・・・ </p>
							</div>
							<p class="r">その他神奈川県・茨城県の一部地域も対応しておりますので、ご相談ください。 </p>
						<!--////////////////-->
					</div>
					<?php include($_SERVER['DOCUMENT_ROOT'] . '/layout/sibar.php'); ?>
				</div>
				<?php include($_SERVER['DOCUMENT_ROOT'] . '/layout/footer.php'); ?>
			</div>

