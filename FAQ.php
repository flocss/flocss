<?php $id="faq"; ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/layout/header.php'); ?>
			<div class="l-breadcrumb">
				<div class="l-container">
					<ul>
						<li><a href="/index.php">ホーム</a></li>
						<li>よくある質問</li>
					</ul>
				</div>
			</div>
			<div class="l-container">
				<!-- code this here -->
				<!--////////////////-->
				<div class="l-main">
					<div class="l-conts">
						<!-- code this here -->
						<div class="p-faq">
							<div class="font">
								<div class="c-title">よくある質問 </div>
							</div>
							<div class="c-box">
								<div class="c-box-title">押し入れ〜1K程度の小さなお部屋の片付けにおすすめ</div>
								<div class="c-box-content">
								<span>はい、自宅・オフィスまで出張買取・回収にお伺い致します。出張買取では、トラック代・燃料費など経費の関係上、出張距離・作業内容等を考慮した上で、出張買取・回収にお伺いできないことがございますが、無料回収または有料引き取りでなら出張可能な場合もございます。この点、御理解いただければと思います。</span>
								<br><br>
								<span>＜出張買取・回収ができない事例＞<br/>「自宅の自転車を一台のみを回収してほしい」等</span>
								</div>
							</div>
							<div class="c-box">
								<div class="c-box-title">Q.　厨房機器・オフィス機器・自宅の不用品を丸ごと回収・買取してほしい。</div>
								<div class="c-box-content">
								<span>はい、リサイクルマスター英雄では、丸ごと回収・買取を御希望の方向けに、「トラック詰め放題パック」というものをご用意しております！「引っ越しに伴い不要なものは全部処分したい」「オフィス用品をまるごと買取・回収してほしい」「厨房機器を格安で処分したい」等の御要望がござましたらお気軽にご相談下さい。安さには自信があります！</span>
								</div>
							</div>
							<div class="c-box">
								<div class="c-box-title">Q.　当日、買取・回収依頼品目が増えても大丈夫ですか。</div>
								<div class="c-box-content">
								<span>はい、大丈夫です。当日担当させていただくスタッフにお気軽にご相談ください。追加品目を含めた料金を再度ご提示させていただきます。</span>
								</div>
							</div>
							<div class="c-box">
								<div class="c-box-title">Q.　引っ越し時の不用品回収依頼は何日前に連絡すればいいですか。</div>
								<div class="c-box-content">
								<span>約2～3週間ぐらい前のご相談が最適ですが、当日対応ができる場合もございます。</span>
								</div>
							</div>
							<div class="c-box">
								<div class="c-box-title">Q.　見積りに費用はかかりますか。</div>
								<div class="c-box-content">
								<span>お見積りは無料で承ります！お気軽にご連絡ください。 </span>
								</div>
							</div>
							<div class="c-box">
								<div class="c-box-title">Q.　とりあえず相談をしたいのですが・・・。</div>
								<div class="c-box-content">
								<span>お見積り・ご相談は無料です！本ページに記載されていないことでも、不明な点等ございましたら、お気軽にご相談ください。２４時間電話 </span>
								</div>
							</div>
						</div>
						<!-- code end here -->

							
						<!--////////////////-->
					</div>
					<?php include($_SERVER['DOCUMENT_ROOT'] . '/layout/sibar.php'); ?>
				</div>
				<?php include($_SERVER['DOCUMENT_ROOT'] . '/layout/footer.php'); ?>
			</div>

