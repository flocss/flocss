				<?php
					if(isset($sibar)){
						$sibar1="";
						$sibar2="";
						$sibar3="";
						$sibar4="";
						$sibar5="";
						$sibar6="";

						if($sibar=="gyoumuyou"){
							$sibar1="is-active";
						}
						if($sibar=="tenpo"){
							$sibar2="is-active";
						}
						if($sibar=="office"){
							$sibar3="is-active";
						}
						if($sibar=="kagukaden"){
							$sibar4="is-active";
						}
						if($sibar=="PCgame"){
							$sibar5="is-active";
						}
						if($sibar=="gomiihin"){
							$sibar6="is-active";
						}	
					}
				?>
					<div class="l-side">
						<img src="/images/common/sibar_1.png">
						<div class="s_box_1">
							<a href="/houjin.php"><div class="s_box_t cc">法人のお客様</div></a>
							<div class="s_box_c rc">
								<ul>
									<a class="<?php echo $sibar1; ?>" href="/houjin_gyoumuyou.php"><img src="/images/footer_arow.png"><li>業務用厨房機器買取・無料回収・<br/>格安処分</li></a>
									<a class="<?php echo $sibar2; ?>" href="/houjin_tenpo.php"><img src="/images/footer_arow.png"><li>店舗用品買取・無料回収・格安処分</li></a>
									<a class="<?php echo $sibar3; ?>" href="/houjin_office.php"><img src="/images/footer_arow.png"><li>オフィス機器買取・無料回収・<br/>格安処分</li></a>
								</ul>
							</div>
							<p class="cc"></p>
						</div>
						<div class="s_box_1">
							<a href="/kojin.php"><div class="s_box_t dd">法人のお客様</div></a>
							<div class="s_box_c rs">
								<ul>
									<a class="<?php echo $sibar4; ?>" href="/kojin_kagukaden.php"><img src="/images/footer_arow2.png"><li>家具家電買取・無料回収</li></a>
									<a class="<?php echo $sibar5; ?>" href="/kojin_PCgame.php"><img src="/images/footer_arow2.png"><li>パソコン・ゲーム機・<br/>小型家電無料回収 </li></a>
									<a class="<?php echo $sibar6; ?>" href="/kojin_gomiihin.php"><img src="/images/footer_arow2.png"><li>不用品回収・ゴミ処分・遺品整理 </li></a>
								</ul>
							</div>
							<p class="dd"></p>
						</div>
						<div class="s_bottom">
							<ul>
								<li>買取・回収の流れ</li>  
								<li>価格表</li>
								<li>よくある質問</li>
								<li>会社概要</li>
							</ul>
						</div>
					</div>
				