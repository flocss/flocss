<?php 
	if(isset($id)){
	
		$gnavi1="";
		$gnavi2="";
		$gnavi3="";
		$gnavi4="";
		$gnavi5="";

		if($id=="index"){
			$gnavi1="active";
		}
		if($id=="flow"){
			$gnavi2="active";
		}
		if($id=="price"){
			$gnavi3="active";
		}
		if($id=="faq"){
			$gnavi4="active";
		}
		if($id=="about"){
			$gnavi5="active";
		}
	}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>flocss</title>
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="stylesheet" type="text/css" href="/css/reset.css">
	<link rel="stylesheet" type="text/css" href="/css/common.css">
	<script type="text/javascript" src="js/common.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
	
<body>
	<div class="index">
		<div class="c-gNav">
			<div class="l-container">
				<div class="logo"><img src="/images/logo.png"></div>
				<ul>
					<li class="<?php echo $gnavi1; ?> <?php echo $gnavi; ?>"><a href="/index.php"><div>ホーム</div><br>Home</a></li>
					<li class="<?php echo $gnavi2; ?>"><a href="/flow.php"><div>買取・回収の流れ</div><br>Flow of Purchase &amp; Collect</a></li>
					<li class="<?php echo $gnavi3; ?>"><a href="/PriceList.php"><div>価格表</div><br>Price List</a></li>
					<li class="<?php echo $gnavi4; ?>"><a href="/FAQ.php"><div>よくある質問</div><br>FAQ</a></li>
					<li class="<?php echo $gnavi5; ?>"><a href="/contactus.php"><div>会社概要</div><br>About Us</a></li>
				</ul>
			</div>
		</div>