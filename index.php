<?php $id="index"; ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/layout/header.php'); ?>
		<div class="l-container">
			<div class="slide">
				<div class="next_prev">
					<span href="" class="prev" onclick="plusSlides(-1)"><img src="/images/index/prev.png"></span>
					<span href="" class="next" onclick="plusSlides(1)"><img src="/images/index/next.png"></span>
				</div>
				<div class="mySlides fade">
					<img src="/images/index/index_background_1.png">
				</div>
				<div class="mySlides fade">
					<img src="/images/index/index_background_1.png">
				</div>
				<div class="mySlides fade">
					<img src="/images/index/index_background_1.png">
				</div>

				<ul>
					<li class="dot" onclick="currentSlide(1)"></li>
					<li class="dot" onclick="currentSlide(2)"></li>
					<li class="dot" onclick="currentSlide(3)"></li>
				</ul>
			</div>
			<section class="section1">
				<div class="img">
					<img src="/images/index/index_background_2.png">
				</div>
				<div class="s_content">
					<div class="s_box">
						<div class="s_title title1">個人さま向け</div>
						<div class="s_content_box">
							<div class="s_content_1">
								<img src="/images/index/s_content_box1.png">
								<div class="s_1">
									<p>粗大 ゴミ・家具・家電・<br/>生活雑貨の処分</p>
									<span>片付けや引越しなどでいらなくなった家庭から出る不用品を回収いたします。</span>
								</div>
							</div>
							<div class="s_content_1">
								<img src="/images/index/s_content_box2.png">
								<div class="s_1">
									<p>ゴミ屋敷整理</p>
									<span>気がついたらいらない物がいっぱいになっていませんか？<br/>何から手をつけていいのか悩んでしまうようなお片づけにも対応いたします。<br/>ワケあり物件もOK です。</span>
								</div>
							</div>
							<div class="s_content_1">
								<img src="/images/index/s_content_box3.png">
								<div class="s_1">
									<p>遺品整理</p>
									<span>お時間がなかなかとれないご家族に代わって大事な思い出の品を大切に処理いたします。</span>
								</div>
							</div>
						</div>
					</div>
					<div class="s_box">
						<div class="s_title title2">個人さま向け</div>
						<div class="s_content_box">
							<div class="s_content_1">
								<img src="/images/index/s_content_box4.png">
								<div class="s_1">
									<p>オフィス家具・事務用品・<br/>OA機器</p>
									<span>オフィス家具から事務用品まで丸ごと回収いたします。</span>
								</div>
							</div>
							<div class="s_content_1">
								<img src="/images/index/s_content_box5.png">
								<div class="s_1">
									<p>作業用工具・業務用厨房機器・<br/>店内用品</p>
									<span>業務用大型機器から小型工具まで丸ごと回収いたします。</span>
								</div>
							</div>
							<div class="s_content_1">
								<img src="/images/index/s_content_box6.png">
								<div class="s_1">
									<p>工場・倉庫・ガレージ・<br/>看板撤去</p>
									<span>工場や倉庫。ガレージ内の不用品を丸ごと回収いたします。</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="section2">
				<div class="s_header_1">
					<span>大好評！</span>
				</div>
				<div class="s_header_2">
					<span>お得な格安トラック積み放題パック</span>
					<img src="/images/index/s_header_1.png">
					<p>まとめて回収！</p>
				</div>
				<div class="s_content1">
					<div class="title">1K程度の小さなお部屋の片付けに</div>
					<div class="content">
						<span>軽（平）トラック1台積み詰め放題</span>
						<p>￥19,000-</p>
						<div class="s"> 1Fの作業員1名（助手1名5,000円〜）</div>
						<div class="s"> 2F〜最大25,000円</div>
						<img src="/images/index/car1.png">
						<div class="font">荷台容積<br/>約<span>2.5</span>平米  </div>
						<div class="a">
							<p class="p">※家電・鋳物のみなら<span>￥16,000-</span></p>
							<p class="g">※不用品回収作業60分程度を基本料金の最低目安にしております。</p>
						</div>
					</div>
				</div>
				<div class="s_content1">
					<div class="title">1K程度の小さなお部屋の片付けに</div>
					<div class="content">
						<span>軽（平）トラック1台積み詰め放題</span>
						<p>￥29,000-</p>
						<div class="s"> 1Fの作業員1名<br/>（助手1名5,000円〜）</div>
						<div class="s"> 2F〜最大35,000円</div>
						<img src="/images/index/car2.png">
						<div class="font">荷台容積<br/>約<span>3.5</span>平米  </div>
						<div class="a">
							<p class="g">※不用品回収作業60分程度を基本料金の最低目安にしております。</p>
						</div>
					</div>
				</div>
				<div class="s_content1">
					<div class="title">1DK〜2DK程度のお部屋の片付けに</div>
					<div class="content">
						<span>2トン平トラック1台積み詰め放題</span>
						<p>￥40,000-</p>
						<div class="s"> 1Fの作業員1名<br/>（助手1名5,000円〜）</div>
						<div class="s"> 2F〜最大50,000円</div>
						<img src="/images/index/car3.png">
						<div class="font">荷台容積<br/>約<span>5.0</span>平米  </div>
						<div class="a">
							<p class="g">※不用品回収作業90分程度を基本料金の最低目安にしております。</p>
						</div>
					</div>
				</div>
				<div class="s_content1">
					<div class="title">1DK〜2DK程度のお部屋の片付けに</div>
					<div class="content">
						<span>2トン平トラック1台積み詰め放題</span>
						<p>￥80,000-</p>
						<div class="s"> 1Fの作業員2名<br/>（助手1名5,000円〜））</div>
						<div class="s"> 2F〜最大95,000円</div>
						<img src="/images/index/car4.png">
						<div class="font">荷台容積<br/>約<span>10</span>平米  </div>
						<div class="a">
							<p class="g">※不用品回収作業120分程度を基本料金の最低目安にしております。</p>
						</div>
					</div>
				</div>
			</section>
		</div >
		<section class="section3">
			<div class="c-title__title">リサイクルマスター英雄はお客様満足度No.1を目指しています </div>
			<div class="l-container">
				<div class="l-main">
					<div class="l-conts">
						<div class="font">
							<span>リサイクルマスター英雄では、法人のお客様を対象とした業務用厨房機器・店舗用品・オフィス機器・オフィス用品の買取、無料回収、格安処分を行っております。また、個人のお客様を対象とした家電家具・パソコン・ゲーム機・不用品・ゴミ・遺品の買取・回収・処分等も幅広く行っております。<br/>引っ越しや店舗移転の際に出る多量の不用品の処分やゴミ屋敷の処分など、不用品でお困りの方の力になりますので、ぜひお気軽にご相談ください。</span>
							<span>また、リサイクルマスター英雄では、「どこよりも高く買い取り、どこよりも安く処分できる」をモットーにお客様の負担を少しでも減らせるように意識しています。対応に関しましても、「即日対応・無料見積り・無料相談・テキパキとした対応・動き」を基軸とし、日々の業務に取り組んでいます。<br/>価格面・対応面の両方において、不用品買取・回収・処分業界における、ナンバーワンの満足度を目指しています。不用品処分をお考えの方は是非一度リサイクルマスター英雄にご相談下さい！お客様の不用品処分を全力でサポート致します！ </span>
							<div class="c-title">初めての方へ</div>
							<span>リサイクルマスター英雄では、法人様から個人様まで、不用品等の買取・無料回収・格安処分等を行っております。<br/>大量の処分品でも当社で分別作業して廃棄致しますのでお時間がない方もお気軽にお申込みください！<br/>※新しい家電・ブランド家具等で良い物は高価買取できる場合もございます。</span>
						</div>
						<div class="c-content">
							<div class="c-title">リサイクルマスター英雄３つの特徴</div>
							<div class="content">
								<div class="c-box">
									<div class="left">
										<span><img src="/images/index/c_box_1.png"></span>
										<p><img src="/images/index/c_box_11.png"></p>
									</div>
									<div class="right">
										<p>同時に丸ごと買取・無料回収・格安処分でお得！</p>
										<span>リサイクルマスター英雄では、多量処分にも対応致します！不用となった電化製品・家具・家電・オフィス用品・業務用厨房機器・パソコン・ゲーム等の処分でお困りでしたら、お気軽にご連絡下さい！不用品の丸ごと買取・無料回収・格安処分が同時にできますので、お時間のない方でも便利でお得です！また、オフィス・店舗をはじめ、家一軒丸ごとの片付け等も行っており、素早く丁寧をモットーに仕事をしております！</span>
										<div class="s">
											<span class="red">丸ごと買取 </span> &nbsp; + &nbsp; <span class="red">無料回収</span> &nbsp; + &nbsp; <span class="red"> 格安処分</span>
										</div>
									</div>
								</div>
								<div class="c-box">
									<div class="left">
										<span><img src="/images/index/c_box_2.png"></span>
										<p><img src="/images/index/c_box_22.png"></p>
									</div>
									<div class="right">
										<p>即日対応可能でお得！</p>
										<span>リサイクルマスター英雄は、即日対応可能です！お電話一本で、不用品を回収致します。まずはお見積りを致しますので、お気軽にお問い合わせ下さい。 </span>
										<div class="s">
											<span class="red">即日対応可能</span>
										</div>
									</div>
								</div>
								<div class="c-box">
									<div class="left">
										<span><img src="/images/index/c_box_3.png"></span>
										<p><img src="/images/index/c_box_33.png"></p>
									</div>
									<div class="right">
										<p>どこよりも高く買い取り、安く処分でお得！</p>
										<span>リサイクルマスター英雄は不用品買取・処分・ゴミ回収の業界No.1を目指しております。無料お見積もり後、万一他社様と比べて1円でも高かった場合は、再度お見積もりをし、お客様が納得できる金額を再度ご提示させて頂きます！ </span>
										<div class="s">
											<span class="red">高価買取 </span> &nbsp; + &nbsp; <span class="red">格安処分</span>
										</div>
									</div>
								</div>
							</div>
							<div class="font">
								<div class="c-title">初めての方へ</div>
								<span>業務用厨房機器・店舗用品・オフィス機器・オフィス用品・PC・ゲーム・家電家具等。リサイクルマスター英雄では法人のお客様から個人</span>
							</div>
							<div class="c-content">
								<div class="c-title__title">回収品目例</div>
								<div class="content">
									<ul>
										<li>洗浄機</li>
										<li>作業台</li>
										<li>業務用冷蔵庫</li>
										<li>シンク</li>
										<li>ガスレンジ</li>
										<li>薬味入れ</li>
										<li>寸銅鍋</li>
										<li>製氷機</li>

										<li>フライヤー</li>
										<li>電子レンジ</li>
										<li>ミキサー</li>
										<li>スライサー</li>
										<li>ショーケース</li>
										<li>コールドテーブル</li>
										<li>小物</li>
										<li>蔵庫</li>

										<li>自動販売機</li>
										<li>券売機 </li>
										<li>ショーケース</li>
										<li>ラック</li>
										<li>レジ</li>
										<li>食器</li>
										<li>OA機器</li>
										<li>パソコン</li>

										<li>机（デスク）</li>
										<li>椅子（チェア）</li>
										<li>カウンター</li>
										<li>ロッカー</li>
										<li>ビジネスフォン</li>
										<li>テーブル</li>
										<li>テレビ</li>
										<li>掃除機</li>

										<li>ソファ</li>
										<li>棚</li>
										<li>タンス</li>
										<li>生活用品</li>
										<li>その他不用品など</li>
									</ul>
									<div class="c-img">
										<img src="/images/index/c_img_1.png">
										<img src="/images/index/c_img_2.png">
										<img src="/images/index/c_img_3.png">
										<img src="/images/index/c_img_4.png">
										<img src="/images/index/c_img_5.png">
										<img src="/images/index/c_img_6.png">
										<img src="/images/index/c_img_7.png">
									</div>
									<div class="z">他にもいろいろお取り扱いしております！</div>
								</div>
							</div>
							<div class="c-title">対応地域一覧</div>
							<div class="map_footer"><img src="/images/index_map.png"></div>
							<div class="c-c red">
								<span class="title_red title_t">東京都</span>
								<p>23区・中央区・港区・新宿区・文京区・台東区・千代田区・墨田区・江東区・品川区・目黒区・大田区・世田谷区・渋谷区・中野区・杉並区・豊島区・北区・荒川区・板橋区・練馬区・足立区・葛飾区・江戸川区・八王子市・田無市・立川市・武蔵野市・三鷹市・府中市・昭島市・調布市・町田市・小金井市・小平市・日野市・東村山市・国分寺市・国立市・西東京市・福生市・狛江市・東大和市・清瀬市・東久留米市・武蔵村山市・多摩市・稲城市・あきる野市・羽村市・青梅市 その他・・・ </p>
							</div>
							<div class="c-c yellow">
								<span class="title_yellow title_t">東京都</span>
								<p>23区・中央区・港区・新宿区・文京区・台東区・千代田区・墨田区・江東区・品川区・目黒区・大田区・世田谷区・渋谷区・中野区・杉並区・豊島区・北区・荒川区・板橋区・練馬区・足立区・葛飾区・江戸川区・八王子市・田無市・立川市・武蔵野市・三鷹市・府中市・昭島市・調布市・町田市・小金井市・小平市・日野市・東村山市・国分寺市・国立市・西東京市・福生市・狛江市・東大和市・清瀬市・東久留米市・武蔵村山市・多摩市・稲城市・あきる野市・羽村市・青梅市 その他・・・ </p>
							</div>
							<div class="c-c xam">
								<span class="title_xam title_t">東京都</span>
								<p>23区・中央区・港区・新宿区・文京区・台東区・千代田区・墨田区・江東区・品川区・目黒区・大田区・世田谷区・渋谷区・中野区・杉並区・豊島区・北区・荒川区・板橋区・練馬区・足立区・葛飾区・江戸川区・八王子市・田無市・立川市・武蔵野市・三鷹市・府中市・昭島市・調布市・町田市・小金井市・小平市・日野市・東村山市・国分寺市・国立市・西東京市・福生市・狛江市・東大和市・清瀬市・東久留米市・武蔵村山市・多摩市・稲城市・あきる野市・羽村市・青梅市 その他・・・ </p>
							</div>
							<p class="r">その他神奈川県・茨城県の一部地域も対応しておりますので、ご相談ください。 </p>
						</div>
					</div>
					<?php include($_SERVER['DOCUMENT_ROOT'] . '/layout/sibar.php'); ?>
				</div>
			<?php include($_SERVER['DOCUMENT_ROOT'] . '/layout/footer.php'); ?>
	   

		 <script type="text/javascript" src="/js/js.js"></script>