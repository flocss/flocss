var gulp = require('gulp');  
var sass = require('gulp-sass');  
var browserSync = require('browser-sync');


var setting = {
  autoprefixer: {
      browser: ['last 2 version', 'Explorer >= 8', 'Android >= 4', 'Android 2.3']
  },
  browserSync: {
    //使わない方はコメントアウトする
    proxy: 'flocss.cd',
    //server:{
    //    baseDir: 'httpdocs',
    //},
  },
//  imagemin: {
//    disabled: true,  // falseでimageminを実行
//    level: 7  // 圧縮率
//  },
// css、jsのミニファイの有効化/無効化
//  minify: {
//    css: false,
//    js: false
//  },
//  cssbeautify: {
//    disabled: true,
//    options: {
//      indent: ''
//    }
//  },
 // csscomb: {
 //   disabled: true,
 // },
  path: {
    base: {
      src: '',
      dest: ''
    },
    sass: {
      src: 'scss/**/*.scss',
      dest: './',
    },
//    js: {
//      src: 'src/assets/js/**/*.js',
//      dest: 'httpdocs/assets/js/',
//    },
//    image: {
//      src: 'src/assets/img/**/*',
//      dest: 'httpdocs/assets/img/',
//    },
//    lib: {
//      src: 'src/assets/lib/**/*',
//      dest: 'httpdocs/assets/lib/',
//    },
//    include: {
//      src: ['src/assets/include/**/*'],
//      dest: 'httpdocs/assets/include/',
//    },
//    etc: {
//      src: 'src/assets/etc/**/*',
//      dest: 'httpdocs/assets/etc/',
//    },
//    html: {
//      src: 'mansion//**/*.php',
//      //src: ['src/**/*', '!src/assets/**/*']
//    },
  }
};
gulp.task('sass', function () {  
    gulp.src('scss/*.scss')
        .pipe(sass({includePaths: ['scss']}))
        .pipe(gulp.dest('css'));
});

gulp.task('browser-sync', function() {  
    browserSync.init(["css/*.css", "js/*.js", "*.html"], {
        server: {
            baseDir: "./"
        }
    });
});

gulp.task('default', ['sass', 'browser-sync'], function () {  
    gulp.watch("scss/*.scss", ['sass']);
    gulp.watch(['./*.php'],　{interval: 0}, browserSync.reload);
    gulp.watch(['./*/*.php'],　{interval: 0}, browserSync.reload);
});