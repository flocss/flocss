<?php include($_SERVER['DOCUMENT_ROOT'] . '/layout/header.php'); ?>
			<div class="l-breadcrumb">
				<div class="l-container">
					<ul>
						<li><a href="/index.php">ホーム</a></li>
						<li><a href="/index.php">個人のお客様</a></li>
						<li>不用品回収・ゴミ処分・遺品整理</li>
					</ul>
				</div>
			</div>
			<div class="l-container">
				<div class="c-06">
					<img src="/images/06/kojin_gomiihin_01.png">
				</div>
			</div>
			<div class="c-title__title_PC"><span class="l-container">不用品回収・ゴミ処分・遺品整理</span></div>
			<div class="l-container">
				<!-- code this here -->
				<!--////////////////-->
				<div class="l-main">
					<div class="l-conts">
						<!-- code this here -->

						<!-- code end here -->
							<div class="font">
								<span>リサイクルマスター英雄では、東京都を拠点として10年あまり、東京都/千葉県/埼玉県/茨城県/神奈川県など関東圏で不用品回収・ゴミ処分・遺品整理に関するお仕事をさせていただいております。不用品の回収、状態の良いものは買い取りもさせていただいております。大量の処分品でも当社で分別作業して廃棄致しますので個人の方はもちろん、法人の方、お時間がない方もお気軽にお申込みください。また、リサイクルマスター英雄は不用品処分、ゴミ回収の業界最安値を目指しております。 まずはフリーダイヤルで無料お見積もりをさせてください。 他社との比較歓迎です！総額で比べてください。万一他社との見積もりと比べて1円でも高かった場合には再度お見積もりさせてください。お客様が納得できる金額を再度ご提示させて頂きます！</span>
							</div>
							<div class="font">
								<div class="c-title">回収品目例</div>
								<img src="/images/06/kojin_gomiihin_02.png">
								<span>回収品目に関しましてはごく一部の例です。品目に関しましてはお気軽にご相談下さい。<br/>また、リサイクルマスター英雄ではゴミ処分や遺品整理も行っております。処分等でお困りでしたらお気軽にご相談下さい<br><br/>処分費用に関しましては価格表をご確認下さい。<br>不用品回収専用のサイトも運営しております。</span>
							</div>
							
							<div class="c-title">対応地域一覧</div>
							<div class="map_footer"><img src="/images/index_map.png"></div>
							<div class="c-c red">
								<span class="title_red title_t">東京都</span>
								<p>23区・中央区・港区・新宿区・文京区・台東区・千代田区・墨田区・江東区・品川区・目黒区・大田区・世田谷区・渋谷区・中野区・杉並区・豊島区・北区・荒川区・板橋区・練馬区・足立区・葛飾区・江戸川区・八王子市・田無市・立川市・武蔵野市・三鷹市・府中市・昭島市・調布市・町田市・小金井市・小平市・日野市・東村山市・国分寺市・国立市・西東京市・福生市・狛江市・東大和市・清瀬市・東久留米市・武蔵村山市・多摩市・稲城市・あきる野市・羽村市・青梅市 その他・・・ </p>
							</div>
							<div class="c-c yellow">
								<span class="title_yellow title_t">東京都</span>
								<p>23区・中央区・港区・新宿区・文京区・台東区・千代田区・墨田区・江東区・品川区・目黒区・大田区・世田谷区・渋谷区・中野区・杉並区・豊島区・北区・荒川区・板橋区・練馬区・足立区・葛飾区・江戸川区・八王子市・田無市・立川市・武蔵野市・三鷹市・府中市・昭島市・調布市・町田市・小金井市・小平市・日野市・東村山市・国分寺市・国立市・西東京市・福生市・狛江市・東大和市・清瀬市・東久留米市・武蔵村山市・多摩市・稲城市・あきる野市・羽村市・青梅市 その他・・・ </p>
							</div>
							<div class="c-c xam">
								<span class="title_xam title_t">東京都</span>
								<p>23区・中央区・港区・新宿区・文京区・台東区・千代田区・墨田区・江東区・品川区・目黒区・大田区・世田谷区・渋谷区・中野区・杉並区・豊島区・北区・荒川区・板橋区・練馬区・足立区・葛飾区・江戸川区・八王子市・田無市・立川市・武蔵野市・三鷹市・府中市・昭島市・調布市・町田市・小金井市・小平市・日野市・東村山市・国分寺市・国立市・西東京市・福生市・狛江市・東大和市・清瀬市・東久留米市・武蔵村山市・多摩市・稲城市・あきる野市・羽村市・青梅市 その他・・・ </p>
							</div>
							<p class="r">その他神奈川県・茨城県の一部地域も対応しておりますので、ご相談ください。 </p>
						<!--////////////////-->
					</div>
					<?php $sibar="gomiihin"; ?>
					<?php include($_SERVER['DOCUMENT_ROOT'] . '/layout/sibar.php'); ?>
				</div>
				<?php include($_SERVER['DOCUMENT_ROOT'] . '/layout/footer.php'); ?>
			</div>

