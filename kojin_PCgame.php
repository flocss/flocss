<?php include($_SERVER['DOCUMENT_ROOT'] . '/layout/header.php'); ?>
			<div class="l-breadcrumb">
				<div class="l-container">
					<ul>
						<li><a href="/index.php">ホーム</a></li>
						<li><a href="/index.php">個人のお客様</a></li>
						<li>パソコン・ゲーム機・小型家電 無料回収</li>
					</ul>
				</div>
			</div>
			<div class="l-container">
				<div class="c-06">
					<img src="/images/06/kojin_PCgame_01.png">
				</div>
			</div>
			<div class="c-title__title_PC"><span class="l-container">パソコン・ゲーム機・小型家電 無料回収</span></div>
			
			<div class="l-container">
				<!-- code this here -->
				<!--////////////////-->
				<div class="l-main">
					<div class="l-conts">
						<!-- code this here -->

						<!-- code end here -->
							
							<div class="p-flow">

							<div class="font">
								<div class="p-kojin-game"><img src="/images/06/kojin_PCgame_02.png"></div>
								<div class="c-title">手順</div>
								<p class="c-content">
									<span>処分可能か<br/>どうかの<br/>チェック</span><img src="	/images/flow/company_2.png">
									<span>包装</span><img src="/images/flow/company_2.png">
									<span>配送</span>
								</span>
							</div>
							<div class="c-box">
								<div class="c-box-left">
									<p>処分可能かどうかのチェック</p>
									<span>＜ＰＣ関係＞<br>○処分可能例<br>タブレット・スマホ・デスクトップ・ノートPC・液晶モニタ・MAC・オフィス・アダプタ・ケーブル・マウス・キーボード・プレイステーション・ニンテンドー・XBOX等<br><br>○処分不可例<br>ブラウン管モニタ・ブラウン管パソコン・テレビ・ビデオ・画面に損傷があるパソコン・パーツのみ・プリンタ・ワープロ・スキャナ等<br><br>その他処分ご希望の品がございましたら下記番号までお気軽にご相談ください。</span>
									<div class="c-box-left__div">
										<div class="c-box-left__title">お気軽にお問い合わせ下さい</div>
										<div class="c-box-left__content">
											<img src="/images/flow/company_4.png">
											<span>0120-485-888</span> 携帯・PHSからもOK!<br/>お電話がつながりにくい場合  070-2177-7772 
											<p>24時間対応</p>
										</div>
									</div>
								</div>
								<div class="c-box-right">
									<img src="/images/06/kojin_PCgame_03.png">
									<img src="/images/06/kojin_PCgame_04.png">
								</div>
							</div>
							<div class="c-box">
								<div class="c-box-img"><img src="/images/flow/company_5.png"></div>
								<div class="c-box-left">
									<p>包装</p>
									<span>製品を段ボールに包装します。<br>＊段ボール代は弊社では負担致しません。<br>＊付属品等はパソコン本体と同じ箱に入れてください。 </span>
								</div>
								<div class="c-box-right">
									<img src="/images/06/kojin_PCgame_05.png">
								</div>
							</div>
							<div class="c-box">
								<div class="c-box-img"><img src="/images/flow/company_5.png"></div>
								<div class="c-box-left">
									<p>配送</p>
									<span>製品を配送します。<br>＊配送連絡は不要です。<br>＊必ずクロネコヤマト「宅急便」をご利用下さい。<br>＊送料は弊社では負担致しません。</span>
								</div>
								<div class="c-box-right">
									<img src="/images/06/kojin_PCgame_06.png">
								</div>
							</div>
						</div>
						<div class="p-kojin-game">
							<div class="p-kojin__title">クロネコヤマト宅急便（お届け先）</div>
							<div class="p-kojin__content">
								<p>郵便番号： 270-2232</p>
								<p>住所：千葉県松戸市和名ヶ谷1333</p>
								<p>電話番号：0120-485-888</p>
								<p>氏名：リサイクルマスター英雄</p>
							</div>
						</div>
						<!--////////////////-->
					</div>
					<?php $sibar="PCgame"; ?>
					<?php include($_SERVER['DOCUMENT_ROOT'] . '/layout/sibar.php'); ?>
				</div>
				<?php include($_SERVER['DOCUMENT_ROOT'] . '/layout/footer.php'); ?>
			</div>

